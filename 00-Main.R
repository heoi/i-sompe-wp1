# _        _____       __  __ _____  ______ 
#(_)      / ____|     |  \/  |  __ \|  ____|
#  _ ______ (___   ___ | \  / | |__) | |__   
#| |______\___ \ / _ \| |\/| |  ___/|  __|  
#| |      ____) | (_) | |  | | |    | |____ 
#|_|     |_____/ \___/|_|  |_|_|    |______|
#by Olivier Heller, olivier.heller@agroscope.admin.ch / 2021-2022
#For more information please consult the README.md

# Declare necessary packages
pkgs_cran <- c("xlsx","config","raster","rgdal","sf","dplyr","sp","readxl","stringr","RColorBrewer")
pkgs_bioc <- c()

# Load packages
source("01-LoadPkgs.R")

# Read config
source("02-ReadConfig.R")

# Initialise
source("03-Init.R")

# Load Paths to data
source("04-LoadPath.R")

#L1 Analysis
# Extraction of Agri4Cast Data for L1
source("06_Extraction_L1_A4C.R")

#L2 Analysis
# Load data for L2 analysis and calculate basic information 
source("05-LoadData_L2.R")
# Extraction of Agri4Cast Data for L2
source("06_Extraction_L2_A4C.R")
# Extraction of Peat and Soil Type Data for L2
source("06_Extraction_L2_Peat_Soil.R")
# Extraction of Soil Property Data for L2
source("06_Extraction_L2_SoilProp.R")

#L3 and L4 Analysis
# Load data for L3 and L4 analysis and calculate basic information 
source("05-LoadData_L4.R")
# Extraction of Agri4Cast Data for L3
source("06_Extraction_L3_A4C.R")
# Extraction of Agri4Cast Data for L4
source("06_Extraction_L4_A4C.R")

# Optional Extraction of Agri4Cast Data for cover crop suitabilty for L4
source("06_Extraction_L4_A4C_CoverCrops.R")

# Extraction of Peat and Soil Type Data for L4
source("06_Extraction_L4_Peat_Soil.R")
# Extraction of Soil Property Data for L4
source("06_Extraction_L4_SoilProp.R")
# Extraction of Slope Data for L4
source("06_Extraction_L4_Slope.R")

#Wrangling and Compiling all results
source("07_Compilation.R")


#### Original Template strucuture
# Explore
## Vizualise
#source("08-VisualiseData.R")
## Model
#source("09-ModelData.R")

# Communicate
## Save Output
source("10-SaveOutput.R")
