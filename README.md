[//]: # (Title: i-SoMPE WP1 R Project)  
[//]: # (Author: Olivier Heller)  
[//]: # (Date: April 20, 2022)  

# i-SoMPE WP1 R Project

## About this R Project
This R projcet was written for the [i-SoMPE](https://isompe.gitlab.io/blog/) project within EJP SOIL in 2021 and 2022 by [Olivier Heller](mailto:olivier.heller@agroscope.admin.ch). It was used to characterize the agro-enviromental zones (AEZ) of Europe based on selected available geo data. The considered geo data includes information on land use, climate, slope and soil. Please consult the report of i-SoMPE for more information on the project and the geo data.

## How to use this Project
### Input data
The input data (AEZs and raster data) to the calculations need to be available to the user. The file path to the input data needs to be customized in the file "04-LoadPath.R". All vector and raster data, except for slope data (file size) and the peat land map (restrictions), are available on [Zenodo.org](https://doi.org/10.5281/zenodo.8224594). The peat land map raster data is available from the original authors [Tanneberger et al., 2017](http://doi.org/10.19189/MaP.2016.OMB.264).


#### Zones
There are three different levels of zonal polygons that need to available to the user:

- path_polygons_L1: Path to the polygons that describe the 13 Environmental zones (EnZ) by [Metzger et al. 2005](https://doi.org/10.1111/j.1466-822X.2005.00190.x)
- path_polygons_L2: Path to the polygons that describe the 146 AEZ of i-SoMPE (intersection of the EnZ and Country borders)
- path_polygons_L4: Path to the polygons that describe the 1006 different agricultural land use classes across the AEZs. The agricultural land use classes were derived from the [CLC 2018](https://land.copernicus.eu/pan-european/corine-land-cover/clc2018).

![Figure 1: AEZ and other levels of i-SoMPE](./img/AEZ_i_Sompe.jpg)

#### Raster data
There are multiple raster data that are needed for the script to run:

- [Agri4Cast](https://agri4cast.jrc.ec.europa.eu/DataPortal/Index.aspx) derived climate data. The calculation of the raster data are done in a separate [R Project](https://gitlab.com/heoi/i-SoMPE-WP1-Agri4Cast).
- Slope data in 7 classes derived from the [EU-DEM v1.0](https://land.copernicus.eu/imagery-in-situ/eu-dem). 
- The peat land map of Europe from [Tanneberger et al., 2017](http://doi.org/10.19189/MaP.2016.OMB.264) 
- Dominant soil type from the Europea soil data base (ESDB) incl. a legend file.
- ESDB-derived soil propertie information:
  - Dominant top soil textural classes
  - Dominant soil water regime
  - Dominant soil rooting depth
  - Dominant top soil gravel content
  
  
### Run the project

1. Download vector and raster data 
2. Adjust paths to data in the "04-LoadPath.R" file
3. Run "00-Main.R"
4. The output will be found in the "output" folder
