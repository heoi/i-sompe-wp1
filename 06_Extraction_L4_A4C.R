##Extract L4 Agri4Cast Climatic

cat("Extracting L4 Agri4Cast data...\n")

#Check if allready existing
if (file.exists(paste(intProdDir,"/","data_A4C_L4.Rda",sep = ""))) {
  load(paste(intProdDir,"/","data_A4C_L4.Rda",sep = ""))
  cat("L4 Agri4Cast data already existing - existing data loaded!\n")

  } else { 
  
  #Defining Dataframe
  data_A4C_L4 <- data_basic_L4
  
  # Load and Stack Agri4Cast-Data
  list_A4C <- list.files(path_Agri4Cast,pattern = "\\.tif$",full.names = TRUE)
  list_A4C <- list_A4C %>%              #exclude variables only relevant to cover crops (if any)
    subset(!grepl('MMDD_WW',list_A4C))
  list_A4C <- list_A4C %>%              #exclude variables only relevant to cover crops (if any)
    subset(!grepl('_CC',list_A4C))
  n_A4C <- length(list_A4C) #Number of A4C files
  stack_A4C <- stack(list_A4C)
  stack_A4C <- clamp(stack_A4C, lower = -99998, useValues=FALSE) #transform -99999 values to NAs
  names_A4C <- tools::file_path_sans_ext(basename(list_A4C)) #extract Name of Raster
  
  #Loop to calculate Means and SD of every Raster for every Polygon
  names_A4C_M <- paste(names_A4C,"_M", sep = "")
  names_A4C_SD <- paste(names_A4C,"_SD", sep = "")
  
  for (i in 1:n_polygons_L4) {
    #extract Polygon
    MPolygon <- st_read(list_polygons_L4[i],quiet = TRUE)
    
    #mean of  rasterdata
    temp_output_M <- extract(stack_A4C, MPolygon, fun = mean ,
                             cellnumbers = TRUE, na.rm = TRUE, weight = FALSE, df = TRUE)
    temp_output_M$ID <- NULL
    data_A4C_L4[i,names_A4C_M] <- temp_output_M
    
    #St. dev. of rasterdata
    temp_output_SD <- extract(stack_A4C, MPolygon, fun = sd ,
                              cellnumbers = TRUE, na.rm = TRUE, weight = FALSE, df = TRUE)
    temp_output_SD$ID <- NULL
    data_A4C_L4[i,names_A4C_SD] <- temp_output_SD
    
    removeTmpFiles(h = 0.0001)
    rm(temp_output_M,temp_output_SD,MPolygon)
    gc()
  }
  
  rm(names_A4C_M,names_A4C_SD,list_A4C,n_A4C,stack_A4C,names_A4C,i)
  
  #save results
  save(data_A4C_L4,file = paste(intProdDir,"/","data_A4C_L4.Rda",sep = ""))
  
  cat("L4 Agri4Cast data extracted!\n")
}
